//
//  TransitionAnimator.swift
//  VideoTransition
//
//  Created by Mac on 13.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit
import AVFoundation

class TransitionAnimator:NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.3
    var presenting = true
    var playerView: VideoPlayerView?

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let fromView = transitionContext.view(forKey: .from)!
        
        let destinitionView = presenting ? toView : fromView
        let departureView = presenting ? fromView : toView

        let originFrame = CGRect(origin: CGPoint(x:containerView.frame.maxX,
                                                 y: 0),
                                 size: containerView.frame.size)
        
        let initialFrame = presenting ? originFrame : destinitionView.frame
        let finalFrame = presenting ? destinitionView.frame : originFrame
        
        let playerOriginFrame = initialFrame
        let playerTranslatedFrame = CGRect(x: 0,
                                           y: 0,
                                           width: containerView.frame.maxX,
                                           height: containerView.frame.height*0.4)
        
        var originPlayerPoint = playerOriginFrame.origin
        originPlayerPoint.x = 0

        let initialPlayerFrame = presenting ? CGRect(origin: originPlayerPoint, size:playerOriginFrame.size) : playerTranslatedFrame
        
        let finalPlayerFrame = presenting ? playerTranslatedFrame : playerOriginFrame
    
        let startColor = presenting ? departureView.backgroundColor : destinitionView.backgroundColor
        let finalColor = presenting ? destinitionView.backgroundColor : departureView.backgroundColor

        if presenting {
            destinitionView.center = CGPoint(
                x: initialFrame.midX,
                y: initialFrame.midY)
            destinitionView.clipsToBounds = true
            
            playerView?.frame = initialPlayerFrame
        }
        destinitionView.backgroundColor = startColor
        departureView.backgroundColor = startColor

        containerView.addSubview(toView)
        containerView.bringSubview(toFront: destinitionView)
        containerView.addSubview(self.playerView!)
        
        UIView.animate(withDuration: duration,
                       animations: {[unowned self] in

                        destinitionView.center = CGPoint(x: finalFrame.midX,
                                                  y: finalFrame.midY)
                        departureView.backgroundColor = finalColor
                        destinitionView.backgroundColor = finalColor

                        self.playerView?.frame = finalPlayerFrame
        },
                       completion:{[unowned self] _ in
                        
                        departureView.backgroundColor = self.presenting ? startColor : finalColor
                        toView.addSubview(self.playerView!)
                        transitionContext.completeTransition(true)
        }
        )
        
    }
    
}
