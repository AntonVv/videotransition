//
//  VideoPlayerView.swift
//  VideoTransition
//
//  Created by Mac on 12.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerView: UIView {

    var player: AVPlayer? {
        set {
            playerLayer.player = newValue
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
        }
        
        get {
            return playerLayer.player
        }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}
