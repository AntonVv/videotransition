//
//  FirstViewController.swift
//  VideoTransition
//
//  Created by Mac on 12.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit
import AVFoundation

struct SourceVideo {
    static let url = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
}
class FirstViewController: UIViewController {

    var playerView: VideoPlayerView?
    let animator = TransitionAnimator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.darkGray
        
        playerView = VideoPlayerView(frame: view.frame)
        playerView?.player = AVPlayer(url: URL(string:SourceVideo.url)!)
        view.addSubview(playerView!)
        playerView?.player?.play()
        
        let tapToMove = UITapGestureRecognizer(target: self, action:#selector(moveGestureTapped))
        view.addGestureRecognizer(tapToMove)
    }
    
    func moveGestureTapped() {
        let second = SecondViewController()
        second.transitioningDelegate = self
        present(second, animated: true, completion: nil)
    }
}

extension FirstViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = true
        animator.playerView = playerView
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = false
        return animator
    }
}
