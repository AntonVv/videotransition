//
//  SecondViewController.swift
//  VideoTransition
//
//  Created by Mac on 12.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        let tapToClose = UITapGestureRecognizer(target: self, action:#selector(close))
        view.addGestureRecognizer(tapToClose)
        
        let textView = UITextView(frame: CGRect(x: 0, y: view.frame.size.height*0.4, width: view.frame.size.width, height: view.frame.size.height * 0.6))
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
        textView.textColor = .white
        textView.backgroundColor = .clear
        textView.isUserInteractionEnabled = false
        view.addSubview(textView)
    }
    
    func close() {
        dismiss(animated: true, completion: nil)
    }
}
